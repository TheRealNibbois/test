---
title: 'Turnkey Linux p1'
published: true
visible: true
---

# Documentation de Turnkey Linux

![logo turnkey](https://distrowatch.com/images/yvzhuwbpy/turnkey.png)

## Le but de cette page

L'objectif de cette page est d'expliquer brièvement l'utilité de Turnkey, ainsi que les multiples petits détails qui y sont lié. Elle va donc respecter le **QQOQCCP** afin d'explorer en détails les fonctionnalités et informations liées au logiciel.

## Turnkey Linux, c'est quoi? Et ça sert à quoi?

Turnkey est un projet **open source** créé en 2008. Turnkey pourrait se traduire par clé en main, ce qui est une très bonne explication de son but : il permet d'installer des logiciels et de les utiliser instantanément. Comment ça marche? C'est plutôt simple: Turnkey contient des logiciels de type **"Virtual Appliance"** qui sont des applications traditionnelles jointe avec la base d'un système d'exploitation. Cela permet de les rendre utilisable sur des serveurs et des machines virtuelles très rapidement et efficacement.**Les logiciels sont donc pré-packagés et prêt à être utilisé directement après leur installation.** Il serait possible de le comparer à Chocolatey, un autre logiciel permettant l'installation de logiciels, mais Turnkey est plus axé serveur. 

![](https://chocolatey.org/content/packageimages/chocolatey-vscode.0.7.1.png)

## Où se trouvent les fichiers? Comment on y accède?

Toutes les images virtuelles disponibles sur Turnkey se trouve sur le [site officiel de Turnkey](turnkeylinux.org) . Sur ce site se trouve des versions stables ainsi que les versions les plus récentes de chacune des images correspondantes aux logiciels recherchés. Il suffit donc de télécharger une version du logiciel recherché et de procéder à son installation.

![](https://www.turnkeylinux.org/files/images/blog/turnkey12-banner.jpg)

[Lire La Suite](turnkey2)