---
title: 'Turnkey p2'
published: true
visible: true
---

[Revenir à la page précédente](../)

## Qu'est-ce qui est disponible?
 ![Logo GitLab](https://assets.gitlab-static.net/assets/gitlab_logo-7ae504fe4f68fdebb3c2034e36621930cd36ea87924c11ff65dbcb8ed50dca58.png?classes=float-left)
 
Turnkey possède plus de **100 images** de logiciels très connus, tel que WordPress, GitLab, etc. La majorité des outils importants de gestion de serveurs sont disponible sur Turnkey. En voici une courte liste: 
* *LAMP STACK
* *WordPress
* *Observium
* *Open VPN
* *FileServer
* *Domain Controller
* *ownCloud
* *Redmine
* *Et bien d'autres! Pour la liste complète, visitez [le site complet](https://www.turnkeylinux.org/all)

![](https://image.flaticon.com/icons/png/128/15/15986.png)

## Pourquoi utiliser Turnkey

Turnkey offre une solution simple à la corvée qu'est installer de nouveaux logiciels ou de nouvelles fonctionnalités sur un serveur, en donnant des images préfaites et prêtes à être utilisées. Cela permet d'accélérer le processus de création et de mise en place d'un serveur. Turnkey vient même avec **sa propre fenêtre de configuration!**

![fenêtre de config](https://upload.wikimedia.org/wikipedia/commons/0/02/Confconsole0.9.4-1.jpg)

## Un peu d'aide

Si vous souhaitez commencer avec Turnkey, voici quelques sources qui pourraient vous aider.
* Un tutoriel Youtube rapide et efficace qui va permettre de *setup* en quelques minutes un serveur utilisant wordpress
[plugin:youtube](https://www.youtube.com/watch?v=vw4o1gOJrbM)
* [La documentation officielle de Turnkey](https://www.turnkeylinux.org/docs)

## Sources consultées

1. [Wikipédia](https://en.wikipedia.org/wiki/TurnKey_Linux_Virtual_Appliance_Library)
2. [Site officiel de Turnkey](https://www.turnkeylinux.org/)
3. [Distrowatch](https://distrowatch.com/table.php?distribution=turnkey)
4. [Tutoriel Youtube](https://www.youtube.com/watch?v=vw4o1gOJrbM)